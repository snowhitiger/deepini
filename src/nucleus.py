import math
import json
import numpy as np
from sampler import Sampler
import matplotlib.pyplot as plt

def woods_saxon(r, radius_fm=6.38, tail=0.535):
    return r*r / (np.exp((r - radius_fm)/tail) + 1)

def deformed_woods_saxon(r, cos_theta, radius_fm=6, beta2=0.0, beta4=0.0, a=0.2):
    cos_theta_sq = cos_theta*cos_theta
    one_div_root_pi = 1 / math.sqrt(np.pi)
    Y20 = math.sqrt(5)/4. * one_div_root_pi * (3.*cos_theta_sq - 1.)
    Y40 = 3./16. * one_div_root_pi * \
        (35.*cos_theta_sq*cos_theta_sq - 30.*cos_theta_sq + 3.)
    Reff =  radius_fm * (1. + beta2*Y20 + beta4*Y40)
    return 1. / (1. + np.exp((r - Reff) / a)), Reff

class Nucleus(object):
    def __init__(self, cfg):
        self.cfg = cfg
        self.num_nucleons = cfg["num_nucleons"]
        self.R = cfg["radius_fm"]
        self.ws_tail = cfg["woods_saxon_tail"]
        self.deformed = cfg["deformed"]

    def max_radius(self):
        '''this is used in inverse sampling'''
        return self.R + 10 * self.ws_tail

    def effective_radius(self):
        '''this is used for collision detecting.
        use max_radius() leads to too many events without a single collision'''
        return self.R + 3 * self.ws_tail

    def sample_nucleons(self):
        pass


class SphericalNucleus(Nucleus):
    def __init__(self, cfg):
        super().__init__(cfg)
        assert(self.deformed == False)

    def sample_nucleons(self):
        A = self.num_nucleons
        sampler = Sampler(woods_saxon,
                extent=[0, self.max_radius()])
        r = sampler.inverse_sample(A)
        cos_theta = 2 * np.random.rand(A) - 1.0
        sin_theta = np.sqrt(1 - cos_theta*cos_theta)
        phi = np.random.rand(A) * 2 * np.pi

        x = r * sin_theta * np.cos(phi)
        y = r * sin_theta * np.sin(phi)
        z = r * cos_theta
        return x, y, z


class DeformedNucleus(Nucleus):
    def __init__(self, cfg):
        super().__init__(cfg)
        assert(self.deformed == True)
        self.beta2 = cfg["deform_beta2"]
        self.beta4 = cfg["deform_beta4"]

    def max_radius(self):
        return self.R*(1. + .63*abs(self.beta2) 
                + .85*abs(self.beta4)) + 10.*self.ws_tail

    def effective_radius(self):
        return self.max_radius() - 7.*self.ws_tail

    def sample_nucleons(self, random_rotate=False, tilt=0, spin=0):
        '''sample nucleons and rotate them by euler_angles;
        tilt: the rotating angle along x
        spin: the rotating angle along z'''

        A = self.num_nucleons
        x = np.zeros(A)
        y = np.zeros(A)
        z = np.zeros(A)
        rmax = self.max_radius()
        for i in range(A):
            while True:
                r = self.max_radius() * (np.random.rand()**(1./3.))
                cos_theta = 2 * np.random.rand() - 1.0
                f = deformed_woods_saxon(r, cos_theta, rmax,
                        self.beta2, self.beta4, self.ws_tail)[0]
                if (np.random.rand() < f):
                    break
            sin_theta = np.sqrt(1 - cos_theta * cos_theta)
            phi = np.random.rand() * 2 * np.pi
            x[i] = r*sin_theta*np.cos(phi)
            y[i] = r*sin_theta*np.sin(phi)
            z[i] = r*cos_theta

        if random_rotate:
            cos_a = 2*np.random.rand() - 1
            sin_a = math.sqrt(1 - cos_a * cos_a)
            spin = 2*np.pi*np.random.rand()
            cos_b = math.cos(spin)
            sin_b = math.sin(spin)
        elif (tilt!=0 or spin!=0):
            cos_a = math.cos(tilt)
            sin_a = math.sqrt(1 - cos_a * cos_a)
            cos_b = math.cos(spin)
            sin_b = math.sin(spin)
        else:
            return x, y, z

        #x_rot = x*cos_b - y*cos_a*sin_b + z*sin_a*sin_b
        #y_rot = x*sin_b + y*cos_a*cos_b - z*sin_a*cos_b
        #z_rot = -y*sin_a + z*cos_a
        x_rot = x*(cos_b*cos_a-sin_b*sin_a) - y*sin_b - z*sin_a*cos_b
        y_rot = x*sin_b*cos_a + y*cos_b - z*sin_a*sin_b
        z_rot = x*sin_a + z*cos_a

        print(cos_a, spin)
        return x_rot, y_rot, z_rot



#### Start testing the above functions by visualization

def save2json(fname, x, y, z):
    import json
    num_nucleons = len(x)
    jstr = {}
    jstr["num_nucleons"] = num_nucleons
    for i in range(num_nucleons):
        jstr["nucleon_%s"%i] = {'x':x[i], 'y':y[i], 'z':z[i]}

    with open(fname, "w") as fp:
        json.dump(jstr, fp)

def plot_spherical_nucleus(nucleus_type="Au"):
    with open("default_config.json", "r") as data:
        cfgs = json.load(data)
    cfg = cfgs[nucleus_type]
    spherical_nucleus = SphericalNucleus(cfg)
    x, y, z = spherical_nucleus.sample_nucleons()
    plt.plot(x, y, 'ro')
    plt.gca().set_aspect("equal")
    plt.show()

def plot_deformed_nucleus(nucleus_type="U", tilt=0, spin=0):
    with open("default_config.json", "r") as data:
        cfgs = json.load(data)
    cfg = cfgs[nucleus_type]
    deformed_nucleus = DeformedNucleus(cfg)
    #x, y, z = deformed_nucleus.sample_nucleons(random_rotate=True)
    x, y, z = deformed_nucleus.sample_nucleons(tilt=tilt, spin=spin)
    plt.plot(x, z, 'ro')
    plt.gca().set_aspect("equal")

    # to make the z along vertical direction in 3d plot
    save2json("../public/%s.json"%nucleus_type, x, y, z)
    plt.show()



def test_woodssaxon():
    import matplotlib.pyplot as plt
    sampler = Sampler(woods_saxon, extent=[0, 12])

    r = sampler.inverse_sample(1970000)
    hist, edges, _ = plt.hist(r, bins=100)
    hist_max = hist.max()

    x = np.linspace(0, 12, 100)
    y = woods_saxon(x)
    y = y * hist_max / y.max()
    plt.plot(x, y, 'r--')
    plt.show()

def test_deformed_ws():
    import matplotlib.pyplot as plt
    cos_theta = np.linspace(-1, 1, 100)
    ws, reff = deformed_woods_saxon(6, cos_theta, beta2=0.4, beta4=-0.2)
    x = reff * cos_theta
    y = reff * np.sqrt(1 - cos_theta*cos_theta)
    plt.plot(x, y, 'ro')
    plt.gca().set_aspect('equal')
    plt.show()

if __name__ == '__main__':
    #test_woodssaxon()
    #test_deformed_ws()
    #plot_spherical_nucleus()
    tilt = np.random.rand() * np.pi
    spin = np.random.rand() * 2 * np.pi
    plot_deformed_nucleus("U", tilt=np.pi/2, spin=0)
