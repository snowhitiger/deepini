import json
import unittest

class Testing(unittest.TestCase):
    def setUp(self):
        with open("../default_config.json", "r") as data:
            self.json = json.load(data)

    def test_cu(self):
        A = 62
        r = 4.2
        js_A = self.json['Cu']['num_nucleons']
        js_r = self.json['Cu']['radius_fm']
        self.assertEqual(A, js_A)
        self.assertEqual(r, js_r)
        self.assertEqual(self.json['Cu']['deformed'], False)

    def test_u2(self):
        A = 238
        r = 6.86
        js_A = self.json['U2']['num_nucleons']
        js_r = self.json['U2']['radius_fm']
        self.assertEqual(A, js_A)
        self.assertEqual(r, js_r)
        self.assertEqual(self.json['U2']['deform_beta2'], 0.265)
        self.assertEqual(self.json['U2']['deform_beta4'], 0.000)
        self.assertEqual(self.json['U2']['deformed'], True)


if __name__ == '__main__':
    unittest.main()
