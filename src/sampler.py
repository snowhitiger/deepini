'''Sample coordiantes of nucleons in nucleus for given distribution.
1. E.g., the proton distribution, the neutron distribution or the nucleons distribution
2. Rejection sampling will be used for deformed nucleus, 
3. Direct sampling using reverse function will be used for spherical nucleus
'''
import math
import numpy as np
from scipy.interpolate import interp1d


class Sampler(object):
    def __init__(self, dist_func, extent=[0, 1]):
        '''Args:
               dist_func: distribution function
               dim: the dimension of the dist_func
        '''
        self.dist_func = dist_func
        self.extent = extent
        assert(len(extent) % 2 == 0)
        self.dim = len(extent) // 2
        self.first_sample = True
        self.inverse_interp = None

    def inverse_sample(self, num, n_cum=1000):
        '''num: number of samplings
        n_cum: number of interpolation nodes'''
        # only do inverse sampling for 1d func
        assert(self.dim == 1)

        if self.first_sample == True:
            x_ = np.linspace(self.extent[0], 
                             self.extent[1],
                             n_cum,
                             endpoint=True,
                             dtype=np.float64)
            fx = self.dist_func(x_)
            cum_fx = np.cumsum(fx)
            # shift the cum_fx to [0, max]
            cum_fx = cum_fx - cum_fx[0]
            # normalize the range to [0, 1)
            cum_fx = cum_fx / cum_fx[-1]
            self.inverse_interp = interp1d(cum_fx, x_, kind='cubic')

        ran = np.random.rand(num)
        sampled_x = self.inverse_interp(ran)
        return sampled_x

    def rejection_sampling(self, *generators):
        while (True): 
            for fi in generators:
                pass



### simple sampling for normal distribution
def normal_dist(x, mu=0, sigma=0.4):
    return 1/(np.sqrt(2*np.pi)*sigma)*np.exp(-(x-mu)**2/(2*sigma**2))

def test_sample_normal():
    import matplotlib.pyplot as plt
    sampler = Sampler(normal_dist, extent=[-3, 3])

    r = sampler.inverse_sample(1970000)
    plt.hist(r, bins=100, normed=True)

    x = np.linspace(-2, 2, 100)
    y = normal_dist(x)
    plt.plot(x, y, 'r--')
    plt.show()



if __name__ == '__main__':
    test_sample_normal()
    #test_woodssaxon()
    #test_deformed_ws()
