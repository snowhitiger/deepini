# DeepIni: A python module to produce fluctuating initial conditions of heavy ion collisions.


Motivation: to study nuclear structure, such as the nuclear deformation, charge/weak charge distribution,
neutron skin and alpha clustering, one has to add various tunable configurations to the initial condition.
The Trento which provides fluctuating initial conditions for heavy ion collisions fail to satiify this
requirement. This python library aims to provide more functionalities for deep learning studies.

Additional to Trento, we will provide the following features,

1. Isolate the data and the code. E.g., the information of nucleus will be saved in one independent json file.
2. Use python for the main code, C for computing heavy part; Javascript for web version.
3. The python version will run on laptop or super cluster; The javascript version will run in web browser. 
4. More output options, such as coordinates of nucleons inside the nucleus; Euler rotatin angles of the whole nucleus; A label to indicate whether one nucleon is participant.
5. Visualize the process in javascrit and html.


## The javascript version for visualization

### Nucleus

-  Select type (U or Au), or set ($\beta_2$, $\beta_4$) mannualy ==> plot nuclear shape
-  Sample nucleons from Woods-Saxon or deformed Woods-Saxon distribution; Optionally from arbitrary distribution for proton and neuteron 
-  Euler Rotations

### Two nucleus
1.  Lorentz Boost
2.  Visualize Collision impace parameter b 
3.  Different collision geometry
4.  Plot (ecc2, s0) scattering plot as a function of collision geometry

### Animation for hydro evolution + hadronic cascade
1.  Dependence on shear/bulk viscosity, EoS, initial entropy fluctuations

### Will it be usful for the field?
1.   Education/visualization
2.   Platform independent event-generator for heavy ion collisions
3.   Supplimentary material for my paper
