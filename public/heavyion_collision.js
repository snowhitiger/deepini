let cfg = {
    "Cu":{
        "num_nucleons":62,
        "num_protons":29,
        "radius_fm":4.20,
        "woods_saxon_tail":0.596,
        "deform_beta2":0.162,
        "deform_beta4":-0.006
    },

    "Au":{
        "num_nucleons":197,
        "num_protons":79,
        "radius_fm":6.38,
        "woods_saxon_tail":0.535,
        "deform_beta2":-0.131,
        "deform_beta4":-0.031
    },

    "Pb":{
        "num_nucleons":208,
        "num_protons":82,
        "radius_fm":6.62,
        "woods_saxon_tail":0.546,
        "deform_beta2":0.0,
        "deform_beta4":0.0
    },

    "U":{
        "num_nucleons":238,
        "num_protons":92,
        "radius_fm":6.81,
        "woods_saxon_tail":0.6,
        "deform_beta2":0.280,
        "deform_beta4":0.093
    },

    "Xe":{
        "num_nucleons":129,
        "num_protons":54,
        "radius_fm":5.42,
        "woods_saxon_tail":0.57,
        "deform_beta2":0.162,
        "deform_beta4":-0.003
    }
};


let canvas;

let impact_par_slider;

let nucleus_type;

let nucleus;
let num_protons;

let neutron;
let proton;
let nucleons;

let radio;

let bmin = 0;
let bmax;

let cross_section = 4.2;

let num_participants;
let num_binary_collisions;
let impact_parameter;
let spatial_eccentricity;

let values_graph;


function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {
    canvas = createCanvas(windowWidth, windowHeight, WEBGL);
    ortho(-width, width, -height, height, 0, 1400);

    radio = createRadio();
    radio.option('Cu', 'Cu');
    radio.option('Au', 'Au');
    radio.option('Pb', 'Pb');
    radio.option('U ', 'U');
    radio.option('Xe', 'Xe');
    radio.position(30, 30);
    radio.style('width', '50px');
    radio.style("font-size", "25px");
    radio.style('color', '#FFFFFF');
    radio._getInputChildrenArray()[2].checked = true;

    impact_par_slider = createSlider(0, 30, 15);
    impact_par_slider.position(width/4, 60);

    let h2 = createElement('h2', 'max impact parameter');
    h2.position(width/4, 0);
    h2.style("color", "#FFFFFF");

    spectatorA = createGraphics(256,256);
    spectatorA.background("#FFFF00");
    
    spectatorB = createGraphics(256,256);
    spectatorB.background("#FF0099");

    texture_par= createGraphics(256,256);
    texture_par.background("#FF6666");

    radio.mouseClicked(redraw);

    impact_par_slider.mouseReleased(redraw);

    values_graph = createGraphics(812, 200);
    values_graph.fill(255);
    values_graph.textSize(36);
    values_graph.textAlign(LEFT);
    noLoop();
}

function draw() {
    //rotateY(frameCount * 0.1);
    background(0);
    pointLight(150, 100, 0, 500, 0, 200);
    pointLight(150, 100, 0, -500, 0, 200);
    pointLight(150, 100, 0, 0, 0, 900);
    directionalLight(204, 204, 204, 200, 0, -900);
 
    nucleus_type = radio.value();
    nucleusA = new DeformedNucleus(cfg[nucleus_type]);
    nucleusB = new DeformedNucleus(cfg[nucleus_type]);

    bmax = impact_par_slider.value();

    collider = new Collider(nucleusA, nucleusB, cross_section);
    one_event = collider.sample_impact_parameter(bmin, bmax);

    num_binary_collisions = one_event[2].length;

    impact_parameter = one_event[3];
    drawData(one_event[0].concat(one_event[1]));

    text_in_webgl();
}

function drawData(nucleons){
    let N = nucleons.length;
    normalMaterial();

    num_participants = 0;

    let y2_minus_x2 = 0.;
    let y2_plus_x2 = 0.;
    let x2, y2;
   
    for (let i=0; i<N; i++) {
        let nuc = nucleons[i];
        push();
        translate(nuc.x*25, nuc.y*25, nuc.z*25);

        if (nuc.participate) {
            texture(texture_par);
            num_participants += 1;

            x2 = nuc.x * nuc.x;
            y2 = nuc.y * nuc.y;

            y2_minus_x2 += y2 - x2;
            y2_plus_x2  += y2 + x2;
        } else {
            texture(spectatorA);
        }
        sphere(35, 20);
        pop();
    }

    let cutoff = 1.0E-7;
    spatial_eccentricity = y2_minus_x2 / (y2_plus_x2 + cutoff);
    //console.log("number of participants = " + num_participants);
}

function text_in_webgl() {
    push();
    //translate(-width + 120, -height+90, 200);
    translate(3*width/4, -0.7*height);
    values_graph.background(0);
    text_value = "number of participants=" + num_participants + "\n" + "number of binary collisions=" + num_binary_collisions + "\n" + "impact parameter b=" + impact_parameter.toFixed(1) + "\n" + "spatial eccentricity=" + spatial_eccentricity.toFixed(2);
    values_graph.text(text_value, 10, 30);
    texture(values_graph);
    plane(812, 200);
    pop();
}
