function deformed_woods_saxon_surface(cos_theta, R, b2, b4) {
    let cos_theta_sq = cos_theta * cos_theta;
    let one_div_root_pi = 1 / Math.sqrt(Math.PI);
    let Y20 = Math.sqrt(5)/4. * one_div_root_pi * (3.*cos_theta_sq - 1.);
    let Y40 = 3./16. * one_div_root_pi * (35.*cos_theta_sq*cos_theta_sq - 30.*cos_theta_sq + 3.);
    return  R * (1. + b2*Y20 + b4*Y40);
}


/**
 * Draw an deformed woods-saxon with given radius, beta2, beta4, beta3
 * @method deformed_ws
 * @param  {Number} [radiusx]         radius of deformed woods-saxon
 * @param  {Number} [beta2]           coefficient for harmonic expansion Y20
 * @param  {Number} [beta4]           coefficient for harmonic expansion Y40
 * @param  {Number} [beta3]           coefficient for harmonic expansion Y30
 * @param  {Integer} [detailX]        number of segments,
 *                                    the more segments the smoother geometry
 *                                    default is 24. Avoid detail number above
 *                                    150, it may crash the browser.
 * @param  {Integer} [detailY]        number of segments,
 *                                    the more segments the smoother geometry
 *                                    default is 16. Avoid detail number above
 *                                    150, it may crash the browser.
 * @chainable
 * @example
 * <div>
 * <code>
 * // draw an deformed nucleus
 * // with radius=100, dormation parameters beta2=0.5, beta4=0.2, beta3=0
 * function setup() {
 *   createCanvas(100, 100, WEBGL);
 * }
 *
 * function draw() {
 *   background(200);
 *   deformed_ws(100, 0.5, 0.2, 0.0);
 * }
 * </code>
 * </div>
 */
p5.prototype.deformed_ws = function(radius, beta2, beta4, beta3, detailX, detailY) {
  this._assert3d('deformed_ws');
  //p5._validateParameters('deformed_ws', arguments);
  if (typeof radius === 'undefined') {
    radius = 50;
  }
  if (typeof beta2 === 'undefined') {
      beta2 = 0;
  }
  if (typeof beta4 === 'undefined') {
      beta4 = 0;
  }

  if (typeof beta3 === 'undefined') {
      beta3 = 0;
  }

  if (typeof detailX === 'undefined') {
      detailX = 20;
  } 
  if (typeof detailY === 'undefined') {
      detailY = 20;
  }

  var gId = 'deformed_ws|' + radius.toFixed(3) + '|' 
                           + beta2.toFixed(3) + '|'
                           + beta4.toFixed(3) + '|'
                           + beta3.toFixed(3) + '|'
                           + detailX + '|' + detailY;

  if (!this._renderer.geometryInHash(gId)) {
    var _deformed_ws = function _deformed_ws() {
      for (var i = 0; i <= this.detailY; i++) {
        var v = i / this.detailY;
        var phi = Math.PI * v - Math.PI / 2;
        var cosPhi = Math.cos(phi);
        var sinPhi = Math.sin(phi);
        let Reff =  deformed_woods_saxon_surface(sinPhi, radius, beta2, beta4);
        for (var j = 0; j <= this.detailX; j++) {
          var u = j / this.detailX;
          var theta = 2 * Math.PI * u;
          var cosTheta = Math.cos(theta);
          var sinTheta = Math.sin(theta);
          var p = new p5.Vector(Reff*cosPhi * sinTheta, Reff*sinPhi, Reff*cosPhi * cosTheta);
          var n = new p5.Vector(cosPhi * sinTheta, sinPhi, cosPhi * cosTheta);
          this.vertices.push(p);
          this.vertexNormals.push(n);
          this.uvs.push(u, v);
        }
      }
    };
    var deformedwsGeom = new p5.Geometry(detailX, detailY, _deformed_ws);
    deformedwsGeom.computeFaces();
    if (detailX <= 24 && detailY <= 24) {
      deformedwsGeom._makeTriangleEdges()._edgesToVertices();
    } else {
      console.log(
        'Cannot draw stroke on deformed woods-saxon with more' +
          ' than 24 detailX or 24 detailY'
      );
    }
    this._renderer.createBuffers(gId, deformedwsGeom);
  }

  this._renderer.drawBuffersScaled(gId, 1, 1, 1);

  return this;
};
