<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <meta name="author" content="Long-Gang Pang, lgpang@mail.ccnu.edu.cn" />
  <meta name="author" content="Kai Zhou, kai@fia.uni-frankfurt.de" />
  <meta name="author" content="Xin-Nian Wang, xnwang@lbl.gov" />
  <meta name="date" content="2019-10-15" />
  <title>Deep learning for nuclear shape deformation</title>
  <style type="text/css">code{white-space: pre;}</style>
  <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>
  <link rel="stylesheet" href="index_style.css">
</head>
<body>
<div id="header">
<h1 class="title">Deep learning for nuclear shape deformation</h1>
<h2 class="author">Long-Gang Pang, lgpang@mail.ccnu.edu.cn</h2>
<h2 class="author">Kai Zhou, kai@fia.uni-frankfurt.de</h2>
<h2 class="author">Xin-Nian Wang, xnwang@lbl.gov</h2>
<h3 class="date">2019-10-15</h3>
</div>
<p align="center">
<img src="heavy_ions.png" alt="drawing" width="800"/>
</p>
<p>Almost all of the heavy nucleus (except the <span class="math inline">\(^{208}_{82} Pb\)</span>) used in high energy heavy ion collisions are deformed. Shown above are the nuclear shapes of widely used Cu, Au, Pb, U and Xe nucleus in heavy ion collisions.</p>
<hr />
<h2 id="deformed-woods-saxon-distribution">Deformed Woods-Saxon distribution</h2>
<p>Shown below is a playground for the nuclear shape deformation.</p>
<p align="center">
<iframe src="plot_deform_surface.html" width=800, height=600></iframe>
</p>
<p>The shapes of most nucleus can be approximated by the following deformed Woods-Saxon distribution,</p>
<p><span class="math display">\[ \rho(r, \theta, \phi) = \frac{\rho_0}{1 + e^{(r - R_0(1 + \beta_2 Y_{20}(\theta) + \beta_4 Y_{40}(\theta)))/a}} \]</span></p>
<p>where the <span class="math inline">\(\rho_0\)</span> is the nucleon density in nucleus, <span class="math inline">\(R_0\)</span> is the Woods-Saxon radius, <span class="math inline">\(\beta_2\)</span> and <span class="math inline">\(\beta_4\)</span> are the deformation parameters. <span class="math inline">\(a\)</span> is the Woods-Saxon tail width.</p>
\begin{align}
Y_{20    } &amp; = \frac{\sqrt{5}}{4\sqrt{\pi}}(3\cos^2\theta - 1) \\
Y_{40} &amp; = \frac{3}{16 \sqrt{\pi}} (35 \cos^4 \theta - 30 \cos^2 \theta + 3)
\end{align}
<p>As the value of <span class="math inline">\(\beta_2\)</span> changes from -0.5 to 0.5, the shape of the nucleus change from pumpkin-like to watermelon-like. As the value of <span class="math inline">\(\beta_4\)</span> changes from -0.2 to 0.2, the north and south pole of the nucleus change from dip to bump.</p>
<hr />
<h2 id="heavy-ion-collisions">Heavy ion collisions</h2>
<p align="center">
<iframe src="heavyion_collision.html" width=800 height=600> </iframe>
</p>
<p>The various steps and concepts involved in the above demonstration are explained below.</p>
<hr />
<h2 id="euler-rotations">Euler Rotations</h2>
<p>Because the deformed nucleus have no spherical symmetry, random rotations will give different collision results. The 3-dimensional rotation matrices along x-axis, y-axis and z-axis are shown below.</p>
<p><span class="math display">\[ R_x(\theta) = \begin{bmatrix} 1 &amp; 0 &amp; 0 \\
                                   0 &amp; \cos \theta &amp; \sin \theta \\
                                   0 &amp; -\sin \theta &amp; \cos \theta 
                   \end{bmatrix}
\]</span></p>
<p><span class="math display">\[ R_y(\theta) = \begin{bmatrix} \cos \theta &amp; 0 &amp; -\sin \theta \\
                                   0 &amp; 1 &amp; 0 \\
                                   \sin \theta &amp; 0 &amp; \cos \theta 
                   \end{bmatrix}
\]</span></p>
<p><span class="math display">\[ R_z(\theta) = \begin{bmatrix} \cos \theta &amp; \sin \theta &amp; 0\\
                                 -\sin \theta &amp; \cos \theta &amp; 0 \\
                                 0 &amp; 0 &amp; 1
                   \end{bmatrix}
\]</span></p>
<p>The Euler rotation matrix using Z-Y-Z order is,</p>
\begin{align}
   &amp; R(\alpha, \beta, \gamma) = R_z(\gamma) R_y(\beta) R_z(\alpha) \\
    = &amp; \begin{bmatrix} \cos \gamma \cos \beta \cos \alpha - \sin \gamma \sin \alpha &amp;
                   \cos \gamma \cos \beta \sin \alpha + \sin \gamma \cos \alpha &amp;
                   - \cos \gamma \sin \beta \\
                   -\sin \gamma \cos \beta \cos \alpha - \cos \gamma \sin \alpha &amp;
                   -\sin \gamma \cos \beta \sin \alpha + \cos \gamma \cos \alpha &amp;
                   \sin \gamma \sin \beta \\
                   \sin \beta \cos \alpha &amp;
                   \sin \beta \sin \alpha &amp;
                   \cos \beta 
                   \end{bmatrix} \\
              = &amp;\begin{bmatrix} R_{11} &amp; R_{12} &amp; R_{13} \\
                                     R_{21} &amp; R_{22} &amp; R_{23} \\
                                     R_{31} &amp; R_{32} &amp; R_{33} 
                  \end{bmatrix}
\end{align}
<p>With the Euler rotation matrix, the new coordinates for (x, y, z) after rotation are,</p>
\begin{align}
x_{rot} &amp; =  R_{11} x + R_{12} y + R_{13} z \\
y_{rot} &amp; =  R_{21} x + R_{22} y + R_{23} z \\
z_{rot} &amp; =  R_{31} x + R_{32} y + R_{33} z
\end{align}
<h2 id="collision-geometry">Collision geometry</h2>
<h3 id="lorentz-boost">Lorentz Boost</h3>
<p>When objects are boosted to high speed, their width along the moving direction will be Lorentz squeezed. At relativistic heavy ion collisions, the speed of the nucleus is close to the speed of light. These nucleus are strongly Lorentz contracted along the beam direction.</p>
<p>One can estimate this velocity for Au+Au <span class="math inline">\(\sqrt{S_NN}=200\)</span> GeV collisions, where 200 GeV is the energy of a pair of nucleons in the local rest frame. As a result, the Lorentz gamma factor for one nucleon is about</p>
<p><span class="math display">\[ \gamma = \frac{\sqrt{s_{NN}}}{2 m_{\rm proton}} \approx 100  \]</span> from where the speed of the Au nucleus is computed <span class="math inline">\(v = 0.999956\)</span> times speed of the light. As a result, the thickness of the nucleus along the beam direction is about 100 times smaller than the radius. The time between first touch and fully overlapped collisions can be neglected.</p>
<p align="center">
<iframe src="lorentz_boost.html" width=800, height=300 frameBorder="0"> </iframe>
</p>
<h3 id="impact-parameter">Impact parameter</h3>
<p>Impact parameter <span class="math inline">\(b\)</span> determines the transverse distance between two nucleus as they approach from two opposite directions. For large <span class="math inline">\(b\)</span>, two nucleus may fly by without a single touch. For small <span class="math inline">\(b\)</span>, two nucleus will partially contact and collide.</p>
<p>The transverse distance in 2-dimensional plane is not sampled uniformly. The probability for impact parameter <span class="math inline">\(b\)</span> is,</p>
<p><span class="math display">\[P(b) db = \frac{2\pi b db}{\pi b_{max}^2} \]</span></p>
<p>So the accumulated probability is,</p>
<p><span class="math display">\[F(b) = \int_{0}^{b} \frac{2\pi \tilde{b} d\tilde{b}}{\pi b_{max}^2} = \frac{b^2}{b_{max}^2}\]</span></p>
<p>So the impact parameter <span class="math inline">\(b = b_{max} \sqrt{F}\)</span> where F is a random number in the range <span class="math inline">\([0, 1)\)</span>.</p>
<h3 id="number-of-binary-collisions-and-number-of-participants">Number of binary collisions and number of participants</h3>
<p>In heavy ion collisions, the &quot;number of participants&quot; refers to the number of participating nucleons and the &quot;number of binary collisions&quot; here refers to the number of nucleon-nucleon collisions. As shown in the following 2 examples, the &quot;number of participants&quot; is 6 for both body-body and tip-tip collisions while &quot;the number of binary collisions&quot; are 3 and 9 respectively.</p>
<p>If the initial entropy deposition is propotional to the linear combination of the &quot;number of participants&quot; and the &quot;number of binary collisions&quot;, the tip-tip collisions will have slightly higher entropy deposition.</p>
<p align="center">
<img src="nbc_vs_nwn.png" width=600>
</p>
<h3 id="spatial-eccentricity">Spatial eccentricity</h3>
<p>In non-central collisions of relativistic heavy ions, the overlapped region has a lemon-like shape. Hot and dense quark gluon plasma is formed in this region. The strong geometric eccentricity at initial state convert to momentum anisotropy at final state, through collective interactions.</p>
<p>The spatial asymmetry is quantified by the spatial eccentricity parameter <span class="math inline">\(\epsilon_x\)</span>,</p>
<p><span class="math display">\[ \epsilon_x = \frac{\langle y^2 - x^2 \rangle}{\langle y^2 + x^2 \rangle} \]</span></p>
<p>where the <span class="math inline">\(\langle \rangle\)</span> stands for averaging weighted with local entropy density.</p>
</body>
</html>
