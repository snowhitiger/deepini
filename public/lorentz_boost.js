let gamma;
let x;
let x_radius;
let y_radius;
let speed_slider;
let loop_iterations = 10000;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(255);

    x = width / 2;
    x_radius = height / 2.;
    y_radius = height / 2.;
}

function draw() {
    background(255);
    lorentzBoost();
}

function lorentzBoost() {
    let beta = (mouseX / width)**(0.1);
    gamma = 1./Math.sqrt(1 - beta*beta);

    noStroke();
    textSize(30);
    text("Move mouse to see the effect of lorentz boost", width/8, height-30);
    text(beta.toFixed(4).toString() + " speed of the light", width/3, 30);
    text("Lorentz factor = " + gamma.toFixed(3), width/3, 60)

    fill("#FFCCCC");
    ellipse(x, height/2, x_radius/gamma, y_radius);
}
