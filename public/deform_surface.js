/// email: lgpang@qq.com
/// Central China Normal University

let beta2_slider;
let beta4_slider;

let  txt2;

let beta2_value_pg;
let beta4_value_pg;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {

    let canvas = createCanvas(windowWidth, windowHeight, WEBGL);
    canvas.position(0, 0);
    ortho(-width, width, -height, height, 0, 900);
    background(255);
    //noLoop();

    orange = createGraphics(256, 256);
    orange.background("#FF6666");
    //orange.text('p', 50, 50);

    beta2_slider = createSlider(0, 100, 0);
    beta4_slider = createSlider(0, 100, 0);

    let y2 = 90;
    let y4 = 120;
    beta2_slider.position(30, y2);
    beta4_slider.position(30, y4);

    let h1 = createElement('h1', 'Playground for deformed nucleus');
    h1.position(30, 0);
    h1.style("color", "#FFFFFF");

    txt2 = createDiv('beta2');
    txt2.position(beta2_slider.x + beta2_slider.width + 10, y2);
    txt2.style("color", "#FFFFFF");

    let txt4 = createDiv('beta4');
    txt4.position(beta4_slider.x + beta4_slider.width + 10, y4);
    txt4.style("color", "#FFFFFF");

    let phelp = createDiv('As beta2 increases from -0.5 to 0.5, the shape of nucleus changes from pumpkin-like to watermelon-like; As beta4 changes from -0.2 to 0.2, the north and south pole changes from dip to bump.');
    phelp.position(30, 9*height/10);
    phelp.style("color", "#999999");

    beta2_value_pg = createGraphics(512, 100);
    beta2_value_pg.fill(255);
    beta2_value_pg.textSize(36);
    beta2_value_pg.textAlign(LEFT);
}

function draw() {
    //rotateY(frameCount * 0.1);
    background(0);

    let beta2 = map(beta2_slider.value(), 0, 100, -0.5, 0.5);
    let beta4 = map(beta4_slider.value(), 0, 100, -0.2, 0.2);

    push();
    //translate(-width + 120, -height+90, 200);
    translate(-width+256+50, 0.7*height);
    beta2_value_pg.background(0);
    text_value = "beta2=" + beta2.toFixed(2) + ",   beta4=" + beta4.toFixed(2);
    beta2_value_pg.text(text_value, 10, 50);
    texture(beta2_value_pg);
    plane(512, 100);
    pop();

    //normalMaterial();

    directionalLight(204, 204, 204, 0, -500, -900);

    pointLight(150, 100, 0, 500, 0, 400);
    pointLight(150, 100, 0, -500, 0, 400);

    pointLight(150, 100, 0, 0, -500, 400);
    pointLight(150, 100, 0, 0, 500, 400);
    pointLight(150, 100, 0, 0, 0, 500);

    rotateZ(frameCount * 0.01);
    rotateX(frameCount * 0.01);
    rotateZ(frameCount * 0.01);

    texture(orange);
    deformed_ws(330, beta2, beta4, 0.0, 20, 20);

    //// orbitControl will rotate the text for beta2, beta4 values
    //orbitControl();
}
