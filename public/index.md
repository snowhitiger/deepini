% Deep learning for nuclear shape deformation
% Long-Gang Pang, lgpang@mail.ccnu.edu.cn; Kai Zhou, kai@fia.uni-frankfurt.de; Xin-Nian Wang, xnwang@lbl.gov
% 2019-10-15

---
header-includes: <link rel="stylesheet" href="index_style.css">
---

<p align="center"> <img src="heavy_ions.png" alt="drawing" width="800"/> </p>

Almost all of the heavy nucleus (except the $^{208}_{82} Pb$) used in high energy heavy ion collisions are deformed.
Shown above are the nuclear shapes of widely used Cu, Au, Pb, U and Xe nucleus in heavy ion collisions.

*** 

## Deformed Woods-Saxon distribution

Shown below is a playground for the nuclear shape deformation.

<p align="center"> <iframe src="plot_deform_surface.html" width=800, height=600></iframe> </p>

The shapes of most nucleus can be approximated by the following deformed Woods-Saxon distribution,
        
$$ \rho(r, \theta, \phi) = \frac{\rho_0}{1 + e^{(r - R_0(1 + \beta_2 Y_{20}(\theta) + \beta_4 Y_{40}(\theta)))/a}} $$

where the $\rho_0$ is the nucleon density in nucleus, $R_0$ is the Woods-Saxon radius, $\beta_2$ and $\beta_4$ are the deformation parameters.
$a$ is the Woods-Saxon tail width.

\begin{align}
Y_{20    } & = \frac{\sqrt{5}}{4\sqrt{\pi}}(3\cos^2\theta - 1) \\
Y_{40} & = \frac{3}{16 \sqrt{\pi}} (35 \cos^4 \theta - 30 \cos^2 \theta + 3)
\end{align}

As the value of $\beta_2$ changes from -0.5 to 0.5, the shape of the nucleus change from pumpkin-like to watermelon-like.
As the value of $\beta_4$ changes from -0.2 to 0.2, the north and south pole of the nucleus change from dip to bump.

*** 
## Heavy ion collisions 

<p align="center"> <iframe src="heavyion_collision.html" width=800 height=600> </iframe> </p>

The various steps and concepts involved in the above demonstration are explained below.

***

## Euler Rotations

Because the deformed nucleus have no spherical symmetry, random rotations will give different collision results.
The 3-dimensional rotation matrices along x-axis, y-axis and z-axis are shown below.

$$ R_x(\theta) = \begin{bmatrix} 1 & 0 & 0 \\
                                   0 & \cos \theta & \sin \theta \\
                                   0 & -\sin \theta & \cos \theta 
                   \end{bmatrix}
$$
                                
$$ R_y(\theta) = \begin{bmatrix} \cos \theta & 0 & -\sin \theta \\
                                   0 & 1 & 0 \\
                                   \sin \theta & 0 & \cos \theta 
                   \end{bmatrix}
$$

$$ R_z(\theta) = \begin{bmatrix} \cos \theta & \sin \theta & 0\\
                                 -\sin \theta & \cos \theta & 0 \\
                                 0 & 0 & 1
                   \end{bmatrix}
$$

The Euler rotation matrix using Z-Y-Z order is,

\begin{align}
   & R(\alpha, \beta, \gamma) = R_z(\gamma) R_y(\beta) R_z(\alpha) \\
    = & \begin{bmatrix} \cos \gamma \cos \beta \cos \alpha - \sin \gamma \sin \alpha &
                   \cos \gamma \cos \beta \sin \alpha + \sin \gamma \cos \alpha &
                   - \cos \gamma \sin \beta \\
                   -\sin \gamma \cos \beta \cos \alpha - \cos \gamma \sin \alpha &
                   -\sin \gamma \cos \beta \sin \alpha + \cos \gamma \cos \alpha &
                   \sin \gamma \sin \beta \\
                   \sin \beta \cos \alpha &
                   \sin \beta \sin \alpha &
                   \cos \beta 
                   \end{bmatrix} \\
              = &\begin{bmatrix} R_{11} & R_{12} & R_{13} \\
                                     R_{21} & R_{22} & R_{23} \\
                                     R_{31} & R_{32} & R_{33} 
                  \end{bmatrix}
\end{align}

With the Euler rotation matrix, the new coordinates for (x, y, z) after rotation are,

\begin{align}
x_{rot} & =  R_{11} x + R_{12} y + R_{13} z \\
y_{rot} & =  R_{21} x + R_{22} y + R_{23} z \\
z_{rot} & =  R_{31} x + R_{32} y + R_{33} z
\end{align}

## Collision geometry

### Lorentz Boost

When objects are boosted to high speed, their width along the moving direction will be Lorentz squeezed.
At relativistic heavy ion collisions, the speed of the nucleus is close to the speed of light.
These nucleus are strongly Lorentz contracted along the beam direction.

One can estimate this velocity for Au+Au $\sqrt{S_NN}=200$ GeV collisions, where 200 GeV is the energy of 
a pair of nucleons in the local rest frame. As a result, the Lorentz gamma factor for one nucleon is about 

$$ \gamma = \frac{\sqrt{s_{NN}}}{2 m_{\rm proton}} \approx 100  $$
from where the speed of the Au nucleus is computed $v = 0.999956$ times speed of the light.
As a result, the thickness of the nucleus along the beam direction is about 100 times smaller than the radius.
The time between first touch and fully overlapped collisions can be neglected.

<p align="center"> <iframe src="lorentz_boost.html" width=800, height=300 frameBorder="0"> </iframe> </p>


### Impact parameter

Impact parameter $b$ determines the transverse distance between two nucleus as they approach from two opposite directions.
For large $b$, two nucleus may fly by without a single touch.
For small $b$, two nucleus will partially contact and collide.

The transverse distance in 2-dimensional plane is not sampled uniformly.
The probability for impact parameter $b$ is,

$$P(b) db = \frac{2\pi b db}{\pi b_{max}^2} $$

So the accumulated probability is,

$$F(b) = \int_{0}^{b} \frac{2\pi \tilde{b} d\tilde{b}}{\pi b_{max}^2} = \frac{b^2}{b_{max}^2}$$

So the impact parameter $b = b_{max} \sqrt{F}$ where F is a random number in the range $[0, 1)$.

### Number of binary collisions and number of participants
In heavy ion collisions, the "number of participants" refers to the number of participating nucleons
and the "number of binary collisions" here refers to the number of nucleon-nucleon collisions.
As shown in the following 2 examples, the "number of participants" is 6 for both body-body and tip-tip collisions 
while "the number of binary collisions" are 3 and 9 respectively.

If the initial entropy deposition is propotional to the linear combination of the "number of participants"
and the "number of binary collisions", the tip-tip collisions will have slightly higher entropy deposition.

<p align="center"> <img src="nbc_vs_nwn.png" width=600> </p>

### Spatial eccentricity

In non-central collisions of relativistic heavy ions, the overlapped region has a lemon-like shape.
Hot and dense quark gluon plasma is formed in this region.
The strong geometric eccentricity at initial state convert to momentum anisotropy at final state,
through collective interactions.

The spatial asymmetry is quantified by the spatial eccentricity parameter $\epsilon_x$,

$$ \epsilon_x = \frac{\langle y^2 - x^2 \rangle}{\langle y^2 + x^2 \rangle} $$

where the $\langle \rangle$ stands for averaging weighted with local entropy density.
