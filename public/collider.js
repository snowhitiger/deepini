// the transverse distance square between two points
function transverse_distance_square(A, B) {
    return (A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y);
}

class Collider {
    constructor(nucleus1, nucleus2, cross_section) {
        this.b = 0;
        this.A1 = nucleus1
        this.A2 = nucleus2
        this.cross_section = cross_section;
        this.cross_sec_r2 = cross_section / Math.PI;
    }

    set_impact_parameter(b) {
        this.b = b;
    }

    max_impact_parameter() {
        this.A1.effective_radius() + this.A2.effective_radius();
    }

    // shift center of nucleons in one nucleus to coord (x, y, z) 
    shift_nucleons_to(nucleons, x, y, z) {
        for (let i=0; i<nucleons.length; i++) {
            nucleons[i].x += x;
            nucleons[i].y += y;
            nucleons[i].z += z;
        }
    }

    // randomly rotate nucleons in one nucleus
    euler_rotation_random(nucleons) {
        let rz_theta1 = 2*Math.PI*Math.random();
        let c1 = Math.cos(rz_theta1);
        let s1 = Math.sin(rz_theta1);

        let ry_theta2 = 2*Math.PI*Math.random();
        let c2 = Math.cos(ry_theta2);
        let s2 = Math.sin(ry_theta2);

        let rz_theta3 = 2*Math.PI*Math.random();
        let c3 = Math.cos(rz_theta3);
        let s3 = Math.sin(rz_theta3);

        let x, y, z, x_rot, y_rot, z_rot;

        for (let i=0; i<nucleons.length; i++) {
            x = nucleons[i].x;
            y = nucleons[i].y;
            z = nucleons[i].z;
            x_rot = x*(c1*c2*c3 - s1*s3) + y*(s1*c2*c3+c1*s3) - z*s2*c3;
            y_rot = x*(-c1*c2*s3 - s1*c3) + y*(-s1*c2*s3+c1*c3) + z*s2*s3;
            z_rot = x*c1*s2 + y*s1*s2 + z*c2;
            nucleons[i].x = x_rot;
            nucleons[i].y = y_rot;
            nucleons[i].z = z_rot;
        }
    }

    participate(A1, A2) {
        let coll = false;
        if (transverse_distance_square(A1, A2) < this.cross_sec_r2) {
            coll = true;
        }
        return coll;
    }

    sample_impact_parameter(bmin, bmax) {
        if (typeof(bmin) === "undefined") {
            bmin = 0;
        }

        if (typeof(bmax) === "undefined") {
            bmax = this.max_impact_parameter();
        }

        console.log('bmax=' + bmax);

        let b;

        let collision = false;
        let nucleons1, nucleons2;

        let binary_collisions = [];
        do {
            b = bmin + (bmax - bmin) * Math.sqrt(Math.random());
            nucleons1 = this.A1.sample_nucleons();
            nucleons2 = this.A2.sample_nucleons();

            this.euler_rotation_random(nucleons1);
            this.shift_nucleons_to(nucleons1, -b/2, 0, 0);
            this.euler_rotation_random(nucleons2);
            this.shift_nucleons_to(nucleons2, b/2, 0, 0);

            let nuc_a, nuc_b;

            if (binary_collisions.length != 0) binary_collisions=[];
            let nn_collide = false;
            for (let i=0; i<nucleons1.length; i++) {
                nuc_a = nucleons1[i];
                for (let j=0; j<nucleons2.length; j++) {
                    nuc_b = nucleons2[j];
                    nn_collide = this.participate(nuc_a, nuc_b);
                    collision = nn_collide || collision;


                    if (nn_collide) {
                        binary_collisions.push([i, j]);
                        nucleons1[i].target_index = j;
                        nucleons2[j].target_index = i;
                    }
                }
            }
        } while (!collision);

        return [nucleons1, nucleons2, binary_collisions, b];
    }
}
