let cfg = {
    "Cu":{
        "num_nucleons":62,
        "num_protons":29,
        "radius_fm":4.20,
        "woods_saxon_tail":0.596,
        "deform_beta2":0.162,
        "deform_beta4":-0.006
    },

    "Au":{
        "num_nucleons":197,
        "num_protons":79,
        "radius_fm":6.38,
        "woods_saxon_tail":0.535,
        "deform_beta2":-0.131,
        "deform_beta4":-0.031
    },

    "Pb":{
        "num_nucleons":208,
        "num_protons":82,
        "radius_fm":6.62,
        "woods_saxon_tail":0.546,
        "deform_beta2":0.0,
        "deform_beta4":0.0
    },

    "U":{
        "num_nucleons":238,
        "num_protons":92,
        "radius_fm":6.81,
        "woods_saxon_tail":0.6,
        "deform_beta2":0.280,
        "deform_beta4":0.093
    },

    "Xe":{
        "num_nucleons":129,
        "num_protons":54,
        "radius_fm":5.42,
        "woods_saxon_tail":0.57,
        "deform_beta2":0.162,
        "deform_beta4":-0.003
    }
};


let canvas;

let nucleus_type;

let nucleus;
let num_protons;

let neutron;
let proton;
let nucleons;

let radio;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {
    canvas = createCanvas(windowWidth, windowHeight, WEBGL);
    ortho(-width, width, height, -height, 0, 1400);
    noLoop();

    radio = createRadio();
    radio.option('Cu', 'Cu');
    radio.option('Au', 'Au');
    radio.option('Pb', 'Pb');
    radio.option('U ', 'U');
    radio.option('Xe', 'Xe');
    radio.position(30, 30);
    radio.style('width', '50px');
    radio.style("font-size", "25px");
    radio.style('color', '#FFFFFF');
    radio._getInputChildrenArray()[0].checked = true;


    // texture for neutron and proton
    neutron = createGraphics(256,256);
    //neutron.background(255, 0, 0);
    //neutron.background("#FFCCCC");
    neutron.background("#FFFF00");
    //neutron.text('n', 50, 50);

    proton = createGraphics(256, 256);
    //proton.background(255, 255, 153);
    proton.background("#FF6666");
    //proton.text('p', 50, 50);

    radio.mouseClicked(redraw);
}

function draw() {
    //rotateY(frameCount * 0.1);
    background(0);
    nucleus_type = radio.value();
    nucleus = new DeformedNucleus(cfg[nucleus_type]);
    drawData(nucleus);
}

function drawData(nucleus){
    nucleons = nucleus.sample_nucleons();
    num_protons = nucleus.num_protons;
    let N = nucleons.length;
    pointLight(150, 100, 0, 500, 0, 200);

    pointLight(150, 100, 0, -500, 0, 200);

    pointLight(150, 100, 0, 0, 0, 900);

    normalMaterial();

    directionalLight(204, 204, 204, 200, 0, -900);
    
    //rotateZ(0.0001);
    //rotateY(frameCount * 0.001);
    //rotateZ(frameCount * 0.01);

    for (let i=0; i<N; i++) {
        let nuc = nucleons[i];
        push();
        translate(nucleons[i].x*35, nucleons[i].y*35, nucleons[i].z*35);
        if (nucleons[i].type == "proton") {
            texture(proton);
        } else {
            texture(neutron);
        }
        sphere(40, 20);
        pop();
    }
}
