function deformed_woods_saxon(r, cos_theta, R, b2, b4, ws_tail) {
    let cos_theta_sq = cos_theta * cos_theta;
    let one_div_root_pi = 1 / Math.sqrt(Math.PI);
    let Y20 = Math.sqrt(5)/4. * one_div_root_pi * (3.*cos_theta_sq - 1.);
    let Y40 = 3./16. * one_div_root_pi * (35.*cos_theta_sq*cos_theta_sq - 30.*cos_theta_sq + 3.);
    let Reff =  R * (1. + b2*Y20 + b4*Y40);
    return 1. / (1. + Math.exp((r - Reff) / ws_tail));
}

class Nucleon {
    // coordinates of nucleon. 
    // type is one of ["proton", "neutron"]
    constructor(x, y, z, type) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.type = type;
        this.collisions = [];
    }

    // usage: nucleon.target_index = idx;
    set target_index(idx) {
        this.collisions.push(idx);
    }

    // usage: pbool = nucleon.participate;
    get participate() {
        if (this.collisions.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    get npart() {
        return this.collisions.length;
    }
}


class Nucleus {
    constructor(cfg) {
        this.cfg = cfg;
        this.num_nucleons = cfg["num_nucleons"];
        this.num_protons = cfg["num_protons"];
        this.R = cfg["radius_fm"];
        this.ws_tail = cfg["woods_saxon_tail"];
    }

    max_radius() {
        //'''this is used in inverse sampling'''
        return this.R + 10 * this.ws_tail;
    }

    effective_radius() {
        //'''this is used for collision detecting.'''
        return this.R + 3 * this.ws_tail;
    }
}


class DeformedNucleus extends Nucleus {
    constructor(cfg) {
        super(cfg);
        this.beta2 = cfg["deform_beta2"];
        this.beta4 = cfg["deform_beta4"];
    }

    max_radius() {
        return this.R * (1. + 0.63 * abs(this.beta2) + 0.85 * abs(this.beta4)) + 10. * this.ws_tail;
    }

    effective_radius() {
        return this.max_radius() - 7. * this.ws_tail;
    }

    sample_nucleons() {
        let nucleons_ = [];
        let A = this.num_nucleons;
        let rmax = this.max_radius();
        let r, cos_theta;
        let type;

        for (let i=0; i<A; i++) {
            do {
                r = rmax * Math.pow(Math.random(), 1./3.);
                cos_theta = 2 * Math.random() - 1.0;
            } while (Math.random() > deformed_woods_saxon(r, cos_theta, rmax, this.beta2, this.beta4, this.ws_tail));
            // do while will repeat as long as the condition is true

            let sin_theta = Math.sqrt(1 - cos_theta * cos_theta);
            let phi = Math.random() * 2 * Math.PI;

            if (i < this.num_protons) {
                type = "proton";
            } else {
                type = "neutron";
            }

            let nuc = new Nucleon(r*sin_theta*Math.cos(phi), r*sin_theta*Math.sin(phi), r*cos_theta, type);
            nucleons_.push(nuc);
            //console.log(nuc);
        }

        return nucleons_;
    }
}
