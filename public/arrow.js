// one arrow, start from (x1, y1), stop at (x2, y2),
function arrow(x1, y1, x2, y2, size) {
    push();
    fill(0);
    strokeWeight(size/3);
    stroke(20);
    line(x1, y1, x2, y2);
    let offset = size;
    let angle = atan2(y1 - y2, x1 - x2);
    translate(x2, y2);
    rotate(angle - HALF_PI);
    triangle(-offset*0.5, offset, offset*0.5, offset, 0, -offset/2);
    pop();
}
