let canvas;
let random_weights = [];
let U;

let neutron;
let proton;

let xSlider, ySlider, zSlider;

let data;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function preload() {
    //data = loadJSON("U.json");
    cfg = loadJSON("default_config.json");
}

function setup() {
    U = DeformedNucleus(cfg.U);
    nucleons = U.sample_nucleons();
    console.log(nucleons);

    createCanvas(windowWidth, windowHeight, WEBGL);
    ortho(-width, width, height, -height, 0, 1000);
    background(255);
    //noLoop();

    // texture for neutron and proton
    neutron = createGraphics(256,256);
    proton = createGraphics(256, 256);
    neutron.background(0, 0, 0);
    neutron.text('n', 50, 50);

    proton.background(255, 205, 205);
    proton.text('p', 50, 50);

    push();
    translate(-width/2, height/2, 0);
    // lignt source position x, y, z
    xSlider = createSlider(0, 500, 0);
    xSlider.position(20, 20);

    ySlider = createSlider(0, 500, 0);
    ySlider.position(20, 40, 0);

    zSlider = createSlider(-1000, 0, -1000);
    zSlider.position(20, 60, 0);

    pop();
}

function draw() {
    //U = loadJSON("U.json", drawData);
    drawData(data);
}

function drawData(data){
    let N = data.num_nucleons;

    const dirY = (mouseY / height - 0.5) * 4;
    const dirX = (mouseX / width - 0.5) * 4;
    //pointLight(255, 255, 255, mouseX, mouseY, 0);
   
    const x = xSlider.value();
    const y = ySlider.value();
    const z = zSlider.value();
    
    push();
    translate(-width/2, height/2, 0);
    textSize(40);
    textAlign(LEFT, CENTER);
    //textFont(inconsolata);
    fill('#222222');
    text("x", 220, 20);
    text("y", 2*ySlider.width, 40);
    text("z", 2*zSlider.width, 60);
    pop();


    directionalLight(204, 204, 204, x, y, z);
    for (let i=0; i<N; i++) {
        let name = "nucleon_" + str(i);
        push();
        translate(data[name].x*30, data[name].z*30, data[name].y*30);
        //if (random() < 82./208.) {
        //    texture(proton);
        //} else {
        //    texture(neutron);
        //}
        sphere(50, 200);
        pop();
    }
}


function redraw() {
    U = loadJSON("U.json", drawData);
}

//function mouseMoved() {
//    redraw();
//}


