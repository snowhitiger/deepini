let x;
let radius = 20;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(255);
    noLoop();
}

function draw() {
    background(255);
    draw_config1(width/4, height/3);
    draw_config2(2*width/3, height/3);

    textSize(20);
    stroke(20);
    push();
    translate(width/4, 2*height/3);
    textAlign(CENTER);
    text("number of binary collisions = 3", 0, 0);
    text("number of participants = 6", 0, 50);
    pop();

    push();
    translate(2*width/3, 2*height/3);
    textAlign(CENTER);
    text("number of binary collisions = 9", 0, 0);
    text("number of participants = 6", 0, 50);
    pop();

}


function draw_3_nucleons(radius) {
    noStroke();
    push();
    let w = 2*radius;
    let h = 6*radius;

    fill("#FF6666");

    ellipse(0, 0, w, h);

    fill("#FFCCCC");

    circle(0, -2*radius, radius);
    circle(0, 0, radius);
    circle(0, 2*radius, radius);

    pop();
}


function draw_config1(x, y) {
    push();
    translate(x, y)
    let arrow_size = 10;
    let offset = 100;
    arrow(-offset + radius, 0, -10, 0, arrow_size);
    arrow(offset - radius, 0, 10, 0, arrow_size);

    push();
    translate(-offset, 0)
    draw_3_nucleons(radius);
    pop();

    push();
    translate(offset, 0)
    draw_3_nucleons(radius);
    pop();

    pop();
}


function draw_config2(x, y) {
    push();
    translate(x, y);
    let arrow_size = 10;
    let h = 4*radius;
    let offset = 100;
    arrow(-offset, h, -20, h, arrow_size);
    arrow(offset, h, 20, h, arrow_size);

    push();
    translate(-offset, 0);
    rotate(PI/2);
    draw_3_nucleons(radius);
    pop();

    push();
    translate(offset, 0);
    rotate(PI/2);
    draw_3_nucleons(radius);
    pop();

    pop();
}
