let canvas;
let random_weights = [];
let U, cfg;
let num_protons;

let neutron;
let proton;
let nucleons;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function preload() {
    cfg = loadJSON("default_config.json");
}

function setup() {
    U = new DeformedNucleus(cfg.U);
    nucleons = U.sample_nucleons();
    num_protons = cfg.U.num_protons;
    //console.log(nucleons);

    createCanvas(windowWidth, windowHeight, WEBGL);
    ortho(-width, width, height, -height, 0, 1200);
    background(0);
    noLoop();

    textSize(100);
    fill("#FF3333");

    // texture for neutron and proton
    neutron = createGraphics(256,256);
    //neutron.background(255, 0, 0);
    //neutron.background("#FFCCCC");
    neutron.background("#FFFF00");
    neutron.text('n', 50, 50);

    proton = createGraphics(256, 256);
    //proton.background(255, 255, 153);
    proton.background("#FF6666");
    proton.text('p', 50, 50);
}

function draw() {
    //rotateY(frameCount * 0.1);
    background(0);
    drawData(nucleons);
}

function redraw() {
    background(255);
    drawData(nucleons);
}

function drawData(nucleons){
    let N = nucleons.length;

    pointLight(150, 100, 0, 500, 0, 200);

    pointLight(150, 100, 0, -500, 0, 200);

    pointLight(150, 100, 0, 0, 0, 900);

    normalMaterial();

    //directionalLight(204, 204, 204, x, y, z);
    directionalLight(204, 204, 204, 200, 0, -900);
    
    let x, y, z;
    for (let i=0; i<N; i++) {
        push();
        let coord = nucleons[i];
        x = coord[0];
        z = coord[1];
        y = coord[2];
        translate(x*30, y*30, z*30);
        if (i < num_protons) {
            texture(proton);
        } else {
            texture(neutron);
        }
        sphere(40, 20);
        pop();
    }
}
