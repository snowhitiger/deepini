/// email: lgpang@qq.com
/// Central China Normal University

let canvas;
let cfg = {
    "Cu":{
        "num_nucleons":62,
        "num_protons":29,
        "radius_fm":4.20,
        "woods_saxon_tail":0.596,
        "deform_beta2":0.162,
        "deform_beta4":-0.006
    },

    "Au":{
        "num_nucleons":197,
        "num_protons":79,
        "radius_fm":6.38,
        "woods_saxon_tail":0.535,
        "deform_beta2":-0.131,
        "deform_beta4":-0.031
    },

    "Pb":{
        "num_nucleons":208,
        "num_protons":82,
        "radius_fm":6.62,
        "woods_saxon_tail":0.546,
        "deform_beta2":0.0,
        "deform_beta4":0.0
    },

    "U":{
        "num_nucleons":238,
        "num_protons":92,
        "radius_fm":6.81,
        "woods_saxon_tail":0.6,
        "deform_beta2":0.280,
        "deform_beta4":0.093
    },

    "Xe":{
        "num_nucleons":129,
        "num_protons":54,
        "radius_fm":5.42,
        "woods_saxon_tail":0.57,
        "deform_beta2":0.162,
        "deform_beta4":-0.003
    }
};


let dx; // x-distance for 5 nucleus
// commonly used nucleus in heavy ion collisions
let nuc_list = ["Cu", "Au", "Pb", "U", "Xe"];
let nuc_color = ["#B87333", "#FFD700", "#808080", "#FF6666", "#2C3539"];
let colors = [];
let captions = [];

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

//function preload() {
//    cfg = loadJSON("default_config.json");
//}

function setup() {
    let canvas = createCanvas(windowWidth, windowHeight, WEBGL);
    canvas.position(0, 0);
    ortho(-width, width, -height, height, 0, 900);
    background(255);
    noLoop();

    dx = width / nuc_list.length;

    for (let i=0; i<nuc_color.length; i++) {
        colors.push(createGraphics(256, 256));
        colors[i].background(nuc_color[i]);

        let nuc = cfg[nuc_list[i]];

        //let txt = nuc_list[i] + ": beta2=" + nuc.deform_beta2 + ", beta4=" + nuc.deform_beta4;
        
        let txt = nuc_list[i];
        captions.push(createDiv(txt));
        captions[i].position((i+0.5)*dx-20, 0.1*height);
        captions[i].style("color", "#FFFFFF");
        captions[i].style("font-size", "36px");
        captions[i].style("align", "center");
    }

    //beta2_value_pg = createGraphics(512, 100);
    //beta2_value_pg.fill(255);
    //beta2_value_pg.textSize(36);
    //beta2_value_pg.textAlign(LEFT);
}

function draw() {
    background(0);

    normalMaterial();

    directionalLight(204, 204, 204, 100, -500, -900);

    pointLight(150, 100, 0, 500, 0, 400);
    pointLight(150, 100, 0, -500, 0, 400);
    pointLight(150, 100, 0, 0, -500, 400);
    pointLight(150, 100, 0, 0, 500, 400);
    pointLight(150, 100, 0, 0, 0, 500);

    //pointLight(150, 100, 0, 900, 0, 300);

    for (let i=0; i<nuc_color.length; i++) {
        push();

        translate(-width+(2*i+1)*dx, 0, 0);

        rotateZ(frameCount * 0.01);
        rotateX(frameCount * 0.01);
        rotateZ(frameCount * 0.01);

        let nuc = cfg[nuc_list[i]]
        texture(colors[i]);

        deformed_ws(0.1*dx*nuc.radius_fm, nuc.deform_beta2, nuc.deform_beta4, 0.0, 24, 24);
        pop();
    }

    //// orbitControl will rotate the text for beta2, beta4 values
    //orbitControl();
}
