// rotate along z-axis for theta1, then along y-axis for theta2 and z-axis for theta3
function euler_rotation(x, y, z, rz_theta1, ry_theta2, rz_theta3) {
    let c1 = Math.cos(rz_theta1);
    let s1 = Math.sin(rz_theta1);
    let c2 = Math.cos(ry_theta2);
    let s2 = Math.sqrt(1 - c2*c2);
    let c3 = Math.cos(rz_theta3);
    let s3 = Math.sin(rz_theta3);
    let x_rot = x*(c1*c2*c3 - s1*s3) + y*(s1*c2*c3+c1*s3) - z*s2*c3;
    let y_rot = x*(-c1*c2*s3 - s1*s3) + y*(-s1*c2*s3+c1*c3) + z*s2*s3;
    let z_rot = x*c1*s2 + y*s1*s2 + z*c2;
    return [x_rot, y_rot, z_rot];
}

// randomly rotate 
function euler_rotation_random(x, y, z) {
    let rz_theta1 = 2*Math.PI*Math.random();
    let c1 = Math.cos(rz_theta1);
    let s1 = Math.sin(rz_theta1);

    let c2 = 2*Math.random() - 1;
    let s2 = Math.sqrt(1 - c2*c2);

    let rz_theta3 = 2*Math.PI*Math.random();
    let c3 = Math.cos(rz_theta3);
    let s3 = Math.sin(rz_theta3);

    let x_rot = x*(c1*c2*c3 - s1*s3) + y*(s1*c2*c3+c1*s3) - z*s2*c3;
    let y_rot = x*(-c1*c2*s3 - s1*s3) + y*(-s1*c2*s3+c1*c3) + z*s2*s3;
    let z_rot = x*c1*s2 + y*s1*s2 + z*c2;
    return [x_rot, y_rot, z_rot];
}
